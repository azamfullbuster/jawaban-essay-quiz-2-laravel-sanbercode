<?php
function hitung($string_data)
{
    $temp_op = preg_replace('([^\\+\\-*\\:%\\^])', ' ', trim($string_data));
    $temp_op = explode(' ', trim($temp_op));

    foreach ($temp_op as $key => $val) {
        if ($val)
            $operators[] = $val;
    }

    $angka = preg_replace('([^0-9])', ' ', trim($string_data));
    $angka = explode(' ', $angka);

    $i = 0;

    foreach ($angka as $key => $val) {
        if ($key == 0) {
            $answer = $val;
            continue;
        }

        if ($val) {
            switch ($operators[$i]) {
                case '+':
                    $answer += $val;
                    break;

                case '-':
                    $answer -= $val;
                    break;

                case '*':
                    $answer *= $val;
                    break;

                case ':':
                    $answer /= $val;
                    break;

                case '%':
                    $answer %= $val;
            }

            $i++;
        }
    }

    return $answer;
}

//TEST CASES
echo hitung("102*2");
echo nl2br("\n");
echo hitung("2+3");
echo nl2br("\n");
echo hitung("100:25");
echo nl2br("\n");
echo hitung("10%2");
echo nl2br("\n");
echo hitung("99-2");
