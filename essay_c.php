//buat tabel customers
CREATE TABLE customers(
customer_id int not null auto_increment,
customer_name varchar(255),
email varchar(255),
password varchar(255),
PRIMARY KEY(customer_id)
);

//buat tabel orders
CREATE TABLE orders(
order_id int not null auto_increment,
amount int,
customer_id int,
PRIMARY KEY(order_id),
FOREIGN KEY(customer_id) REFERENCES customers(customer_id)
);