//menghitung total amount tiap customer dengan descending order pada total amount
SELECT customers.customer_name, SUM(orders.amount) AS 'total_amount'
FROM customers JOIN orders
WHERE customers.customer_id = orders.customer_id
GROUP BY customers.customer_name ORDER BY 'total_amount' DESC;